module gitlab.com/OrcaXS/uexky-go

go 1.14

require (
	github.com/facebookgo/ensure v0.0.0-20160127193407-b4ab57deab51 // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20150612182917-8dac2c3c4870 // indirect
	github.com/globalsign/mgo v0.0.0-20180403085842-f76e4f9da92e
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/go-cmp v0.2.0
	github.com/graph-gophers/graphql-go v0.0.0-20181022152218-b174f9eaf050
	github.com/julienschmidt/httprouter v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mailgun/mailgun-go v1.1.1
	github.com/onsi/ginkgo v1.7.0 // indirect
	github.com/onsi/gomega v1.4.3 // indirect
	github.com/opentracing/opentracing-go v1.0.2 // indirect
	github.com/pkg/errors v0.8.0
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20181017193950-04a2e542c03f // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

